import Qs from 'query-string'


class ApiResponse {
  constructor(resp) { this.resp = resp }
  map = (valueF) => { valueF(this.resp); return this }
  mapError = (errorF) => this
}

export const ErrorType = Object.freeze({
  CONTTECTION_ERROR: 0,
  SERVER_ERROR: 1,
  NOT_FOUND: 2,
  API_ERROR: 3,
  UNKNOWN: 4,
})
class ApiError {
  constructor(error, errorType) {
    this.error = error
    this.type = errorType
  }
  map = (valueF) => this
  mapError = (errorF) => { errorF(this); return this }
}

export const apiFecth = async (route, config) => {
  try {
    const resp = await fetch(route, {
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json'
      },
      ...config
    })
    const decoded = await resp.json(resp)

    switch (resp.status) {
      case 200: return new ApiResponse(decoded)
      case 404: return new ApiError(decoded, ErrorType.NOT_FOUND)
      case 500: return new ApiError(decoded, ErrorType.SERVER_ERROR)
      default: return new ApiError(decoded, ErrorType.UNKNOWN)
    }
  } catch (e) {
    if (e instanceof TypeError) {
      return new ApiError(null, ErrorType.CONTTECTION_ERROR)
    } else if (e instanceof SyntaxError) {
      return new ApiError(null, ErrorType.SERVER_ERROR)
    }
  }
}

const getUri = (route, params) => {
  const uri = "http://" + process.env.NEXT_PUBLIC_API_URI
  params = Qs.stringify(params)
  return params ? `${uri}/${route}?${params}` : `${uri}/${route}`
}

export const apiGet = (route, params) => apiFecth(getUri(route, params))
export const apiPost = (route, body, params) => apiFecth(
  getUri(route, params),
  {
    method: 'POST',
    body: JSON.stringify(body)
  }
)

