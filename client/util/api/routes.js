
export const GET_ALL_PERMISSIONS = () => "user/permissions"
export const GET_ALL_ROLES = () => "user/roles"

export const CREATE_ROLE = () => "user/role/create"
export const UPDATE_ROLE = roleName => `user/role/${roleName}/update`
export const DELETE_ROLE = roleName => `user/role/${roleName}/delete`
export const GET_ROLE = roleName => `user/role/${roleName}`

export const CREATE_USER = "user/create"
export const UPDATE_USER = userName => `user/${userName}/update`
export const DELETE_USER = userName => `user/${userName}/delete`
export const GET_USER = userName => `user/${userName}`

