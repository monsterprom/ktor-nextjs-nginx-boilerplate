
import * as Routes from './routes'
import { apiGet, apiPost } from './methods'

export const getAllPermissions = () => apiGet(Routes.GET_ALL_PERMISSIONS())
export const getAllRoles = () => apiGet(Routes.GET_ALL_ROLES())

/* Role endpoints */
export const createRole = (name, slug, description, permissions) =>
    apiPost(Routes.CREATE_ROLE(), {name, slug, description, permissions})

export const getRole = roleName => apiGet(Routes.GET_ROLE(roleName))

export const updateRole = (name, slug, description, permissions) =>
    apiGet(Routes.UPDATE_ROLE(name), { slug, description, permissions })

export const deleteRole = roleName => apiPost(Routes.DELETE_ROLE(roleName))


/* User endpoints */
export const createUser = (username, roles, email) =>
    apiPost(Routes.CREATE_ROLE(), { username, roles, email })

export const getUser = username => apiGet(Routes.GET_USER(username))

export const updateUser = (username, roles, email) =>
    apiGet(Routes.UPDATE_USER(username), { username, roles, email })

export const deleteUser = username => apiPost(Routes.DELETE_USER(username))

