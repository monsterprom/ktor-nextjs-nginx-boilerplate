import { Form, Input, Button } from 'antd'

import SignLayout from '@/components/layout/Sign'
import Branding from '@/components/Branding'

export default function LoginPage() {
  return (
    <SignLayout
      image="/image/login.jpg"
    >
      <Branding />
      <Form layout="vertical" hideRequiredMark={true}>
        <Form.Item
          label="Имя пользователя или e-mail"
          name="username"
          rules={[
            { required: true, message: "Это поле обязательно для заполнения" }
          ]}
        >
          <Input placeholder="example@example.com" />
        </Form.Item>
        <Form.Item
          label="Пароль"
          name="username"
          rules={[
            { required: true, message: "Это поле обязательно для заполнения" }
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item className="mt-4">
          <Button type="primary" htmlType="submit">
            Войти
          </Button>
        </Form.Item>
      </Form>
    </SignLayout>
  )
}

