import { useState } from 'react'
import Link from 'next/link'
import {
  Row, Col,
  Radio,
  Space,
  Table,
  Typography,
  Card,
  Button
} from 'antd'
import { PlusOutlined } from '@ant-design/icons'

import DashboardLayout from '@/components/layout/Dashboard'
import ContentWrap from '@/components/ContentWrap'
import PageHeader from '@/components/PageHeader'

const columns = [
  { title: "Логин", sorter: true },
  { title: "Имя" },
  { title: "Роль", sorter: true },
  { title: "Email", sorter: true },
  { title: "Действия" },
]

const paginationOptions = ['10', '20', '30']

const { Text } = Typography

export default function UsersList() {
  const [isPending, setPending] = useState(true)
  const [data, setData] = useState([])
  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: paginationOptions[0]
  })

  return (
    <DashboardLayout>
      <PageHeader title="Управление аккаунтами" />
      <ContentWrap>
        <Card>
          <Row className="mb-4" justify="space-between">
            <Col>
              <Link href="/users/create" passHref>
                <Button
                  type="primary"
                  icon={<PlusOutlined />}
                >
                  Добавить польщователя
                </Button>
              </Link>
            </Col>
            <Col>
              <Space>
                <Text>Показывать</Text>
                <Radio.Group
                  disabled={isPending}
                  options={paginationOptions}
                  optionType="button"
                  buttonStyle="solid"
                  value={pagination.pageSize}
                  onChange={
                    e => setPagination({ current: 1, pageSize: e.target.value })
                  }
                />
              </Space>
            </Col>
          </Row>
          <Table
            style={{width: '100%'}}
            dataSource={data}
            pagination={pagination}
            columns={columns}
            emptyText="Нет данных"
            loading={isPending}
          />
        </Card>
      </ContentWrap>
    </DashboardLayout>
  )
}

