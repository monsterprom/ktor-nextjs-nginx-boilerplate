import { useState } from 'react'
import {
  Row, Col,
  Table,
  Form,
  Input,
  Typography,
  Card,
  Button,
  Divider
} from 'antd'

import DashboardLayout from '@/components/layout/Dashboard'
import ContentWrap from '@/components/ContentWrap'
import PageHeader from '@/components/PageHeader'

const columns = [
  { title: "ID" },
  { title: "Описание" },
]

const { Text } = Typography

const formLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    //flex: 'none',
    span: 14,
  }
}

const RolesList = ({ permissions, value, onChange }) => {
  return (
    <Table
      loading={true}
      columns={[
        { title: "Наименование", dataIndex: "name" },
        { title: "ID", dataIndex: "slug" },
        { title: "Описание", dataIndex: "description" },
      ]}
      rowSelection={{
        onChange: onChange,
        type: "checkbox"
      }}
    />
  )
}

export default function UserCreate() {
  const [isPending, setPending] = useState(true)
  const [data, setData] = useState([])

  return (
    <DashboardLayout>
      <PageHeader title="Добавить пользователя" />
      <ContentWrap>
        <Card>
          <Form layout="horizontal" {...formLayout}>
            <Form.Item
              name="username"
              label="Логин"
              rules={[
                { required: true, message: "Это поле обязательно для заполнения" }
              ]}
            >
              <Input placeholder="admin"/>
            </Form.Item>
            <Form.Item
              name="password"
              label="Пароль"
              rules={[
                { required: true, message: "Это поле обязательно для заполнения" }
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Divider />
            <Form.Item
              name="email"
              label="Электронный адрес"
            >
              <Input placeholder="admin@example.com"/>
            </Form.Item>
            <Form.Item label="Фамилия, имя" style={{ marginBottom: 0 }}>
              <Form.Item
                name="lastname"
                style={{
                  display: "inline-block",
                  width: "calc(50% - 8px)"
                }}
              >
                <Input placeholder="Иванов"/>
              </Form.Item>
              <Form.Item
                name="firstname"
                style={{
                  display: "inline-block",
                  width: "calc(50% - 8px)",
                  marginLeft: "16px"
                }}
              >
                <Input placeholder="Иван"/>
              </Form.Item>
            </Form.Item>
            <Form.Item
              name="description"
              label="Описание"
            >
              <Input.TextArea
                placeholder="Администратор ресурса, знакомы с детства"
              />
            </Form.Item>
            <Form.Item
              name="roles"
              label="Роли"
            >
              <RolesList />
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 6 }}>
              <Button type="primary" htmlType="submit">
                Создать пользователя
              </Button>
            </Form.Item>
          </Form>
        </Card>
      </ContentWrap>
    </DashboardLayout>
  )
}




