import { useState, useEffect } from 'react'
import {
  Row, Col,
  Space,
  Table,
  Form,
  Input,
  Checkbox,
  Typography,
  Card,
  Button
} from 'antd'
import { PlusOutlined } from '@ant-design/icons'

import DashboardLayout from '@/components/layout/Dashboard'
import ContentWrap from '@/components/ContentWrap'
import PageHeader from '@/components/PageHeader'

import * as api from '../../../util/api'

const columns = [
  { title: "ID" },
  { title: "Описание" },
]

const formLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    //flex: 'none',
    span: 14,
  }
}

const insertKey = permissions => permissions.map(item => {
  item.key = item.id
  return item
})

const PermissionList = ({ onChange }) => {
  const [permissions, setPermissions] = useState([])
  const [isPending, setPending] = useState(true)
  useEffect(() => {
    (async () => {
      const response = await api.user.getAllPermissions()
      response.map(({ permissions }) => {
        setPending(false)
        setPermissions(insertKey(permissions))
      })
      .mapError(error => {
        console.log(error)
      })
    })()
  }, [])

  return (
    <Table
      dataSource={permissions}
      loading={isPending}
      columns={[
        { title: "Наименование", dataIndex: "name" },
        { title: "ID", dataIndex: "id" },
        { title: "Описание", dataIndex: "description" },
      ]}
      rowSelection={{
        onChange: onChange,
        type: "checkbox"
      }}
    />
  )
}

export default function RoleCreate() {
  const [isPending, setPending] = useState(true)

  const submit = async ({ name, slug, description, permissions }) => {
    setPending(true)
    const response = await api.user.createRole(
      name, slug, description, permissions
    )

    response
      .map(() => setPending(false))
      .mapError(error => {
        setPending(false)
        console.log(error)
      })
  }

  return (
    <DashboardLayout>
      <PageHeader title="Добавить роль" />
      <ContentWrap>
        <Card>
          <Form
            layout="horizontal"
            onFinish={submit}
            {...formLayout}
          >
            <Form.Item
              name="name"
              label="Наименование"
              rules={[
                { required: true, message: "Это поле обязательно для заполнения" }
              ]}
            >
              <Input placeholder="Администратор"/>
            </Form.Item>
            <Form.Item
              name="slug"
              label="ID"
              rules={[
                { required: true, message: "Это поле обязательно для заполнения" }
              ]}
            >
              <Input placeholder="admin"/>
            </Form.Item>
            <Form.Item
              name="description"
              label="Описание"
            >
              <Input.TextArea
                placeholder="Группа пользователей с максимальными привилегиями"
              />
            </Form.Item>
            <Form.Item
              name="permissions"
              label="Разрешения"
            >
              <PermissionList />
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 6 }}>
              <Button type="primary" htmlType="submit">
                Создать роль
              </Button>
            </Form.Item>
          </Form>
        </Card>
      </ContentWrap>
    </DashboardLayout>
  )
}




