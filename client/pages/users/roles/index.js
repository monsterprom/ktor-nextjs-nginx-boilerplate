import { useEffect, useState } from 'react'
import Link from 'next/link'
import {
  Row, Col,
  Radio,
  Space,
  Table,
  Typography,
  Card,
  Button,
  Popconfirm,
  message
} from 'antd'
import { PlusOutlined, EditFilled, DeleteFilled } from '@ant-design/icons'

import DashboardLayout from '@/components/layout/Dashboard'
import ContentWrap from '@/components/ContentWrap'
import PageHeader from '@/components/PageHeader'

import * as api from '../../../util/api'


const paginationOptions = ['10', '20', '30']

const { Text } = Typography


const ActionButtons = ({ role, onBeforeConfirm, onAfterConfirmClosed }) => {
  const [isConfirmVisible, toggleConfirm] = useState(false)
  const [isPending, setPending] = useState(false)

  const onEditRole = () => {
    console.log(slug)
  }

  const onDeleteRole = async () => {
    setPending(true)
    const response = await api.user.deleteRole(role.slug)
    response
      .map(() => {
        onHideConfirm()
        message.success(`Роль "${role.name}" была удалена`)
        setPending(false)
      })
      .mapError(() => {
        onHideConfirm()
        message.error(`Не удалось удалить роль "${role.name}"`)
        setPending(false)
      })
  }

  const onShownCofirm = () => !onBeforeConfirm() && toggleConfirm(true)
  const onHideConfirm = () => {
    toggleConfirm(false)
    onAfterConfirmClosed()
  }

  return (
    <Space>
      <Button size="small"
        icon={<EditFilled />}
        onClick={() => onEditRole(slug)}
      >
        Изменить
      </Button>
      <Popconfirm
        placement="rightTop"
        title={`Вы точно хотите удалить роль "${role.name}"?`}
        okText="Да"
        cancelText="Отмена"
        visible={isConfirmVisible}
        onConfirm={onDeleteRole}
        okButtonProps={{loading: isPending}}
        onCancel={onHideConfirm}
        cancelButtonProps={{disabled: isPending}}
      >
        <Button
          size="small"
          icon={<DeleteFilled />}
          onClick={() => onShownCofirm()}
        />
      </Popconfirm>
    </Space>
  )
}

export default function RolesList() {
  const [isPending, setPending] = useState(true)
  const [isConfirmVisible, toggleConfirm] = useState(false)
  const [data, setData] = useState([])
  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: paginationOptions[0]
  })

  useEffect(() => {
    (async () => {
      const response = await api.user.getAllRoles()
      response
        .map(({ roles }) => {
          setPending(false)
          setData(roles)
        })
        .mapError(error => {
          setPending(false)
          console.log(error)
        })
    })()
  }, [])


  const columns = [
    {
      title: "Наименование",
      dataIndex: "name",
      sorter: true
    },
    {
      title: "ID",
      dataIndex: "slug"
    },
    {
      title: "Разрешения",
      dataIndex: "permissions",
      render: value => value.join(", ")
    },
    {
      title: "Описание",
      dataIndex: "description"
    },
    {
      title: "Действия",
      align: "right",
      render: role => <ActionButtons
        role={role}
        onBeforeConfirm={() => {
          toggleConfirm(true);
          return isConfirmVisible 
        }}
        onAfterConfirmClosed={() => toggleConfirm(false)}
      />
    },
  ]

  return (
    <DashboardLayout>
      <PageHeader title="Роли и разрешения" />
      <ContentWrap>
        <Card>
          <Row className="mb-4" justify="space-between">
            <Col>
              <Link href="/users/roles/create" passHref>
                <Button
                  type="primary"
                  icon={<PlusOutlined />}
                >
                  Добавить роль
                </Button>
              </Link>
            </Col>
            <Col>
              <Space>
                <Text>Показывать</Text>
                <Radio.Group
                  disabled={isPending}
                  options={paginationOptions}
                  optionType="button"
                  buttonStyle="solid"
                  value={pagination.pageSize}
                  onChange={
                    e => setPagination({ current: 1, pageSize: e.target.value })
                  }
                />
              </Space>
            </Col>
          </Row>
          <Table
            dataSource={data}
            pagination={pagination}
            columns={columns}
            loading={isPending}
          />
        </Card>
      </ContentWrap>
    </DashboardLayout>
  )
}


