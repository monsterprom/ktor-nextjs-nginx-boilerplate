import PageHeader from '@/components/PageHeader'
import DashboardLayout from '@/components/layout/Dashboard'

export default function Home() {
  return (
    <DashboardLayout>
      <PageHeader title="Панель управления" />
    </DashboardLayout>
  )
}
