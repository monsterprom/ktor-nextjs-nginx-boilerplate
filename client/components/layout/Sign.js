import { Layout, Row, Col } from 'antd'

export default function SignLayout({ children, image, title }) {
  return (
    <Layout style={{height: '100vh'}}>
      <Row style={{height: '100%'}}>
        <Col span={12} className="sign-image">
          <img src={image} />
        </Col>
        <Col span={8} offset={2} className="sign-content">
          {children}
        </Col>
      </Row>
    </Layout>
  )
}

