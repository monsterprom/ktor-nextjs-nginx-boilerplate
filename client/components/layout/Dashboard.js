import { useState } from 'react'
import { Layout, Menu, Row, Col, Button } from 'antd'
import {
  LeftOutlined,
  RightOutlined
} from '@ant-design/icons'

import Sidebar from '@/components/Sidebar'
import UserBadge from '@/components/UserBadge'

const { Header, Sider, Content } = Layout

const CollapseButton = ({ onClick, collapsed }) => (
  <Button
    icon={collapsed ? <RightOutlined /> : <LeftOutlined />}
    onClick={onClick}
  />
)

export default function DashboardLayout({ children }) {
  const [sideCollapsed, setSideCollapsed] = useState(false)

  return (
    <Layout style={{height: "100vh"}}>
      <Sidebar isCollapsed={sideCollapsed} />
      <Layout>
        <Header>
          <Row justify="space-between">
            <Col>
              <CollapseButton
                collapsed={sideCollapsed}
                onClick={() => setSideCollapsed(!sideCollapsed)}
              />
            </Col>
            <Col>
              <UserBadge />
            </Col>
          </Row>
        </Header>
        <Content style={{ overflow: 'auto' }}>
          {children}
        </Content>
      </Layout>
    </Layout>
  )
}

