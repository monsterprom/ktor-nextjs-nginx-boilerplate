import { Typography } from 'antd'
import Head from 'next/head'
import ContentWrap from '@/components/ContentWrap'

const { Title } = Typography

export default function PageHeader({ title }) {
  return (
    <div className="page-header">
      <Head>
        <title>{title}</title>
      </Head>
      <ContentWrap>
        <Title>{title}</Title>
      </ContentWrap>
    </div>
  )
}

