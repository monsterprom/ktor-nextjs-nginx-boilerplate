import Link from 'next/link'

import SmallLogo from './logo/small'
import NormalLogo from './logo/normal'

export default function Branding({ isCollapsed }) {
  return (
    <div className="branding">
      <Link href="/" passHref>
        <a>{ isCollapsed ? <SmallLogo /> : <NormalLogo /> }</a>
      </Link>
    </div>
  )
}

