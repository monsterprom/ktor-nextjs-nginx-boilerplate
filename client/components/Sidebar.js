import Link from 'next/link'
import { Layout, Menu, Typography } from 'antd'
import {
  DatabaseOutlined,
  UserOutlined,
} from '@ant-design/icons'
import Branding from '@/components/Branding'

const { Sider } = Layout
const { Text } = Typography

const menu = [
  {
    title: "Главное",
    slug: "main",
    items: [
      {
        uri: "/",
        slug: "dashboard",
        text: "Панель управления",
        icon: <DatabaseOutlined />
      },
      {
        slug: "users",
        text: "Пользователи",
        icon: <UserOutlined />,
        submenu: [
          {
            uri: "/users/roles",
            slug: "roles",
            text: "Роли и разрешения"
          },
          {
            uri: "/users",
            slug: "accounts",
            text: "Управление аккаунтами"
          },
        ]
      },
    ]
  },
]

const SectionTitle = ({ text }) => (
  <li className="sidebar-section-title">
    <Text strong>{text}</Text>
  </li>
)

const composeSubMenu = (groupSlug, parent) => {
  return parent.submenu.map(item => {
      return (
        <Menu.Item key={`${groupSlug}_${parent.slug}_${item.slug}`}>
          <Link href={item.uri}>
            {item.text}
          </Link>
        </Menu.Item>
      )
  })
}

const composeMenu = () => menu.map(group => {
  return [
    <SectionTitle text={group.title}/>,
    group.items.map(item => {
      if (item.submenu) {
        return (
          <Menu.SubMenu 
            key={`${group.slug}_${item.slug}`} 
            icon={item.icon}
            title={item.text}
          >
            {composeSubMenu(group.slug, item)}
          </Menu.SubMenu>
        )
      }

      return (
        <Menu.Item 
          key={`${group.slug}_${item.slug}`} 
          icon={item.icon}
        >
          <Link href={item.uri}>
            {item.text}
          </Link>
        </Menu.Item>
      )
    })
  ]
})

export default function Sidebar({ isCollapsed }) {
  return (
    <Sider theme="light" width={300} collapsed={isCollapsed}>
      <Branding isCollapsed={isCollapsed}/>
      <Menu mode="inline">
        {composeMenu(menu)}
      </Menu>
    </Sider>
  )
}

