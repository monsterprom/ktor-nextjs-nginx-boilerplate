import { Menu, Dropdown } from 'antd'
import { DownOutlined } from '@ant-design/icons'

export default function UserBadge() {
  const menu = (
    <Menu>
      <Menu.Item key="logout">Мои данные</Menu.Item>
      <Menu.Item key="logout">Выйти из аккаунта</Menu.Item>
    </Menu>
  )

  return (
    <Dropdown overlay={menu} placement="bottomRight">
      <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
        John Doe <DownOutlined />
      </a>
    </Dropdown>
  )
}

