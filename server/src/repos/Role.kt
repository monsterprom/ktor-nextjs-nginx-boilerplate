package com.server.repos

import arrow.core.Either
import arrow.core.right
import com.fasterxml.jackson.annotation.JsonInclude
import com.server.repos.backend.mongo.MongoConnection
import com.server.repos.backend.mongo.MongoRepository
import com.server.repos.common.*
import org.litote.kmongo.*

data class Role(
    val name: String,
    val slug: String,

    @JsonInclude(JsonInclude.Include.NON_NULL)
    val description: String?,

    val permissions: List<String> = emptyList()
)

interface RoleRepo: Repository<Role> {
//    fun deleteRole(slug: String): IO<Role?>
//    suspend fun getPermissions(roles: List<String>): Either<FindError, Set<String>>
}

class RoleRepoMongo(conn: MongoConnection): RoleRepo, MongoRepository<Role>(conn, Role::class.java) {

//    override suspend fun getPermissions(roles: List<String>): Either<FindError, Set<String>> =
//        if (roles.isEmpty()) emptySet<String>().right()
//        else find(Role::slug `in` roles).map {
//            roles -> roles.fold(emptySet()) { acc, it -> acc.union(it.permissions) }
//        }

//    override fun getIndices(): Map<String, Collection<FieldOption>> = mapOf(
//        "name" to listOf(FieldOption.UNIQUE),
//        "slug" to listOf(FieldOption.UNIQUE),
//    )

//    override fun deleteRole(slug: String): IO<Role?> =
//        IO { getCollection().findOneAndDelete(Role::slug eq slug) }
}