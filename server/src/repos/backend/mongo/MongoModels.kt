package com.server.repos.backend.mongo

import arrow.core.Either
import arrow.core.computations.*
import arrow.core.*
import com.mongodb.BasicDBObject
import com.mongodb.client.model.*
import com.server.repos.common.Patch
import com.server.repos.common.RecordID
import com.server.repos.common.RepoRecord
import com.server.repos.common.WriteOperation
import org.bson.conversions.Bson
import org.litote.kmongo.setValue

class RecordIDException(id: RecordID): Exception("Alien record ID: $id")

fun <T> RepoRecord<T>.getIDQuery() =
    when (id) {
        is MongoRecordID -> BasicDBObject("_id", (id as MongoRecordID).id)
        else -> throw RecordIDException(id)
    }

fun <T> Patch<T>.toMongoPatch(): List<Bson> = when (this) {
    is Patch.Set<*> -> values.map { setValue(it.key, it.value) }
}

fun <T> WriteOperation<T>.toMongoModel(): WriteModel<T> = when (this) {
    is WriteOperation.Insert<*> ->
        InsertOneModel(record as T)

    is WriteOperation.DeleteRecord<*> ->
        DeleteOneModel(record.getIDQuery())
    is WriteOperation.DeleteOne<*> ->
        DeleteOneModel(predicate.toMongoFilter())
    is WriteOperation.DeleteMany<*> ->
        DeleteManyModel(predicate.toMongoFilter())

    is WriteOperation.ReplaceRecord<*> ->
        ReplaceOneModel(record.getIDQuery(), replacement as T)
    is WriteOperation.Replace<*> ->
        ReplaceOneModel(predicate.toMongoFilter(), replacement as T)

    is WriteOperation.UpdateRecord<*> ->
        UpdateOneModel(record.getIDQuery(), patch.toMongoPatch())
    is WriteOperation.UpdateOne<*> ->
        UpdateOneModel(predicate.toMongoFilter(), patch.toMongoPatch())
    is WriteOperation.UpdateMany<*> ->
        UpdateManyModel(predicate.toMongoFilter(), patch.toMongoPatch())
}
