package com.server.repos.backend.mongo

import com.mongodb.client.*
import org.litote.kmongo.KMongo

data class MongoConnection(val client: MongoClient, val db: MongoDatabase) {
    companion object {
        fun fromProps(host: String, user: String, password: String, dbName: String): MongoConnection {
            val uri = "mongodb://$user:$password@$host"
            return fromUri(uri, dbName)
        }

        fun fromUri(uri: String, dbName: String): MongoConnection {
            val client = KMongo.createClient(uri)
            return MongoConnection(client, client.getDatabase(dbName))
        }
    }
}
