package com.server.repos.backend.mongo

import com.mongodb.client.model.Filters
import com.server.repos.common.Predicate
import com.server.repos.common.PredicateOperation
import com.server.repos.common.PredicateRelation
import org.bson.BsonDocument
import org.bson.Document
import org.bson.conversions.Bson
import org.litote.kmongo.EMPTY_BSON

fun <T> Predicate.Property<T>.toMongoFilter(): Bson =
    when (relation) {
        PredicateRelation.EQ -> Filters.eq(property, value)
        PredicateRelation.NEQ -> Filters.ne(property, value)
        PredicateRelation.GT -> Filters.gt(property, value)
        PredicateRelation.GTE -> Filters.gte(property, value)
        PredicateRelation.LT -> Filters.lt(property, value)
        PredicateRelation.LTE -> Filters.lte(property, value)
        PredicateRelation.CONTAINS -> Filters.eq(property, value)
        PredicateRelation.IN -> Filters.`in`(property, value)
    }

fun Predicate.toMongoFilter(): Bson =
    when (this) {
        is Predicate.Property<*> -> this.toMongoFilter()
        is Predicate.Expression -> {
            val fn = when(op) {
                PredicateOperation.AND -> combineFilters(Filters::and)
                PredicateOperation.OR -> combineFilters(Filters::or)
                PredicateOperation.NOT -> { args ->
                    Filters.not(combineFilters(Filters::and)(args))
                }
                PredicateOperation.NOR -> { args ->
                    Filters.not(combineFilters(Filters::or)(args))
                }
            }

            fn(args.map{ it.toMongoFilter() })
        }
    }

internal fun combineFilters(combiner: (List<Bson>) -> Bson): (Iterable<Bson?>) -> Bson = {
    filters ->
    filters
        .filterNotNull()
        .filterNot { it is Document && it.isEmpty() }
        .filterNot { it is BsonDocument && it.isEmpty() }
        .run {
            when {
                isEmpty() -> EMPTY_BSON
                size == 1 -> first()
                else -> combiner(this)
            }
        }
}
