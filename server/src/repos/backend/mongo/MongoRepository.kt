package com.server.repos.backend.mongo

import arrow.core.*
import arrow.syntax.function.pipe
import com.cesarferreira.pluralize.pluralize
import com.cesarferreira.pluralize.utils.Plurality
import com.mongodb.BasicDBObject
import com.mongodb.ErrorCategory
import com.mongodb.MongoBulkWriteException
import com.mongodb.client.model.*
import com.server.repos.common.*
import com.server.repos.common.Predicate
import org.bson.BsonDocument
import org.bson.types.ObjectId

import org.litote.kmongo.*

data class MongoRecordID(val id: ObjectId): RecordID()

abstract class MongoRepository<T>(
    protected val conn: MongoConnection,
    dataClass: Class<T>,
    protected val codec: MongoCodec<T> = DefaultMongoCodec(
        dataClass,
        conn.db.codecRegistry
    ),
    overrideCollectionName: String? = null,
): Repository<T> {

    private val collectionName = overrideCollectionName ?: genCollectionName(dataClass)
    private val collection = conn.db.getCollection(collectionName)
    private val typedCollection = conn.db.getCollection(collectionName, dataClass)

    class NotFoundException: Exception()
    class CodecDecodingException(message: String): Exception(message)


    private fun convertFindException(e: Throwable) = when (e) {
        is NotFoundException -> FindError.NotFound
        is CodecDecodingException -> FindError.InvalidSchema(e.toString())
        else -> FindError.Unknown(e.toString())
    }


    override suspend fun find(vararg predicates: Predicate?, pagination: Pagination?)
    : Either<FindError, RepoRecordNEL<T>> = Either.catch {
        val findIterable = collection.find(foldPredicate(predicates).toMongoFilter())
        if (pagination != null) {
            findIterable.skip(pagination.skip)
            findIterable.limit(pagination.limit)
        }

        val result = findIterable
            .map {
                when (val decoded = codec.decode(it)) {
                    is Either.Left -> throw CodecDecodingException(decoded.a.message)
                    is Either.Right -> decoded.b
                }
            }
            .toList()

        if (result.isEmpty()) {
            throw NotFoundException()
        }

        NonEmptyList.fromListUnsafe(result)
    }.mapLeft(::convertFindException)


    override suspend fun findAll(pagination: Pagination?): Either<FindError, RepoRecordNEL<T>> =
        find(null, pagination = pagination)


    override suspend fun findOne(vararg predicate: Predicate?)
    : Either<FindError, RepoRecord<T>> = Either.catch {
        val result = collection.findOne(foldPredicate(predicate).toMongoFilter())
            ?: throw NotFoundException()

        when (val parsed = codec.decode(result)) {
            is Either.Left -> throw CodecDecodingException(parsed.a.message)
            is Either.Right -> parsed.b
        }
    }.mapLeft(::convertFindException)


    override suspend fun insert(records: NonEmptyList<T>) : WriteErrorNEL? =
        records.map { WriteOperation.Insert(it) }.pipe { bulkWrite(it) }


    override suspend fun insertOne(record: T): WriteError? =
        insert(record.nel())?.head


    override suspend fun deleteOne(record: RepoRecord<T>): WriteError? =
        bulkWrite(WriteOperation.DeleteRecord(record).nel())?.head


    override suspend fun deleteOne(vararg predicate: Predicate?): WriteError? =
        bulkWrite(WriteOperation.DeleteOne<T>(foldPredicate(predicate)).nel())?.head


    override suspend fun delete(records: RepoRecordNEL<T>): WriteErrorNEL? =
        records.map { WriteOperation.DeleteRecord(it) }.pipe { bulkWrite(it) }


    override suspend fun delete(vararg predicate: Predicate?): WriteErrorNEL? =
        bulkWrite(WriteOperation.DeleteMany<T>(foldPredicate(predicate)).nel())


    override suspend fun replace(records: NonEmptyList<Pair<RepoRecord<T>, T>>): WriteErrorNEL? =
        records.map { WriteOperation.ReplaceRecord(it.first, it.second) }.pipe { bulkWrite(it) }


    override suspend fun replaceOne(record: RepoRecord<T>, replacement: T): WriteError? =
        bulkWrite(WriteOperation.ReplaceRecord(record, replacement).nel())?.head


    override suspend fun replaceOne(replacement: T, vararg predicate: Predicate?): WriteError? =
        bulkWrite(
            WriteOperation.Replace(foldPredicate(predicate), replacement).nel()
        )?.head


    override suspend fun update(records: NonEmptyList<Pair<RepoRecord<T>, Patch<T>>>)
    : WriteErrorNEL? =
        records.map { WriteOperation.UpdateRecord(it.first, it.second) }.pipe { bulkWrite(it) }


    override suspend fun update(patch: Patch<T>, vararg predicate: Predicate?)
    : WriteErrorNEL? =
        bulkWrite(WriteOperation.UpdateMany<T>(foldPredicate(predicate), patch).nel())


    override suspend fun updateOne(patch: Patch<T>, vararg predicate: Predicate?)
    : WriteError? =
        bulkWrite(WriteOperation.UpdateOne<T>(foldPredicate(predicate), patch).nel())?.head


    override suspend fun updateOne(record: RepoRecord<T>, patch: Patch<T>)
    : WriteError? =
        bulkWrite(WriteOperation.UpdateRecord(record, patch).nel())?.head


    private fun convertBulkWriteException(e: MongoBulkWriteException)
            : WriteErrorNEL = when {
        e.writeErrors.isNotEmpty() -> {
            NonEmptyList.fromListUnsafe(e.writeErrors.map {
                when (it.category) {
                    ErrorCategory.EXECUTION_TIMEOUT -> WriteError.Timeout(it.message)
                    ErrorCategory.DUPLICATE_KEY -> WriteError.Duplicate(it.message)
                    else -> WriteError.UnknownError(it.message)
                }
            })
        }
        e.writeConcernError != null -> WriteError.Permitted(e.toString()).nel()
        else -> WriteError.UnknownError(e.toString()).nel()
    }


    override suspend fun bulkWrite(operations: Iterable<WriteOperation<T>>): WriteErrorNEL? =
        Either.catch {
            operations
                .map { it.toMongoModel() }
                .pipe {
                    typedCollection.bulkWrite(it)
                }
        }.pipe {
            when (it) {
                is Either.Left -> when (val e = it.a) {
                    is MongoBulkWriteException -> convertBulkWriteException(e)
                    else -> null
                }
                else -> null
            }
        }


    companion object {
        fun <T> genCollectionName(t: Class<T>) =
            t.simpleName
                .split("(?=\\p{Upper})".toRegex())
                .filter { it.isNotEmpty() }
                .map { it.toLowerCase() }
                .pipe {
                    it.dropLast(1) + listOf(it.takeLast(1).first().pluralize(Plurality.Singular))
                }
                .joinToString(".")

    }

}


