package com.server.repos.backend.mongo

import arrow.core.*
import com.mongodb.MongoClientSettings
import com.server.repos.common.RepoRecord
import com.server.repos.common.record
import org.bson.Document
import org.bson.codecs.DecoderContext
import org.bson.codecs.EncoderContext
import org.bson.codecs.configuration.CodecRegistries.fromProviders
import org.bson.codecs.configuration.CodecRegistries.fromRegistries
import org.bson.codecs.configuration.CodecRegistry
import org.bson.codecs.pojo.PojoCodecProvider
import org.bson.json.JsonWriter
import org.bson.types.ObjectId
import java.io.StringWriter

data class DTODecodingError(val message: String)

interface MongoCodec<T> {
    fun decode(document: Document): Either<DTODecodingError, RepoRecord<T>>
}

class DefaultMongoCodec<T>(
    private val dataClass: Class<T>,
    private val registry: CodecRegistry
): MongoCodec<T> {

    private val codec = registry.get(dataClass)

    override fun decode(document: Document): Either<DTODecodingError, RepoRecord<T>> = try {
        val bson = document.toBsonDocument(dataClass, registry)
        val result = codec.decode(bson.asBsonReader(), DecoderContext.builder().build())
        val id = document.get("_id", ObjectId::class.java)

        result.record(MongoRecordID(id)).right()
    } catch (e: Throwable) {
        DTODecodingError(e.toString()).left()
    }
}
