package com.server.repos.user

import com.server.repos.backend.mongo.MongoConnection
import com.server.repos.backend.mongo.MongoRepository

class UserRepoMongo(conn: MongoConnection) : UserRepo, MongoRepository<User>(conn, User::class.java) {

//    override fun getIndices(): Map<String, Collection<FieldOption>> = mapOf(
//        "username" to listOf(FieldOption.UNIQUE),
//        "email" to listOf(FieldOption.UNIQUE, FieldOption.OPTIONAL),
//    )

//    override fun pullRole(users: List<User>, roleSlug: String): UpdateResult = update(
//        User::username `in` users.map { it.username },
//        pull(User::roles, roleSlug)
//    )
//
//    override fun assignRole(users: List<User>, roleSlug: String): UpdateResult = update(
//        and(User::username `in` users.map{ it.username }, not(User::roles.contains(roleSlug))),
//        push(User::roles, roleSlug)
//    )
//
//    override fun refreshPermissions(users: List<User>) {
//        collection.find()
//        TODO("Not yet implemented")
//    }
}
