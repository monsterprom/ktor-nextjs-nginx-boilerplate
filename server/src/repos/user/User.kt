package com.server.repos.user

import com.server.repos.common.*

data class User(
    val username: String,
    val email: String?,
    val roles: List<String> = emptyList(),
)

interface UserRepo: Repository<User> {
//    fun pullRole(users: List<User>, roleSlug: String): UpdateResult
//    fun assignRole(users: List<User>, roleSlug: String): UpdateResult
//    fun refreshPermissions(users: List<User>)
}

