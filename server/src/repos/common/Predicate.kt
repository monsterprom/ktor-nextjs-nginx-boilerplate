package com.server.repos.common

// Relying on KMongo implementation for now
import arrow.syntax.function.pipe
import org.litote.kmongo.path
import kotlin.internal.OnlyInputTypes
import kotlin.reflect.*

enum class PredicateRelation {
    EQ, NEQ, GT, GTE, LT, LTE, CONTAINS, IN
}

enum class PredicateOperation {
    AND, OR, NOT, NOR
}

sealed class Predicate {
    data class Property<T>(
        val property: String,
        val value: T?,
        val relation: PredicateRelation
    ): Predicate()

    data class Expression(
        val args: Iterable<Predicate>,
        val op: PredicateOperation
    ): Predicate()
}

infix fun <@OnlyInputTypes T> KProperty<T?>.eq(value: T?) =
    Predicate.Property<T>(path(), value, PredicateRelation.EQ)

infix fun <@OnlyInputTypes T> KProperty<T?>.neq(value: T?) =
    Predicate.Property<T>(path(), value, PredicateRelation.NEQ)

infix fun <@OnlyInputTypes T> KProperty<T?>.gt(value: T?) =
    Predicate.Property<T>(path(), value, PredicateRelation.GT)

infix fun <@OnlyInputTypes T> KProperty<T?>.gte(value: T?) =
    Predicate.Property<T>(path(), value, PredicateRelation.GTE)

infix fun <@OnlyInputTypes T> KProperty<T?>.lt(value: T?) =
    Predicate.Property<T>(path(), value, PredicateRelation.LT)

infix fun <@OnlyInputTypes T> KProperty<T?>.lte(value: T?) =
    Predicate.Property<T>(path(), value, PredicateRelation.LTE)

infix fun <@OnlyInputTypes T> KProperty<T?>.`in`(value: Iterable<T?>) = null

infix fun <@OnlyInputTypes T> KProperty<Iterable<T?>>.contains(value: T?) =
    Predicate.Property<T>(path(), value, PredicateRelation.CONTAINS)

fun foldPredicate(predicate: Array<out Predicate?>): Predicate =
    predicate
        .filterNotNull()
        .pipe { Predicate.Expression(it, PredicateOperation.AND)  }