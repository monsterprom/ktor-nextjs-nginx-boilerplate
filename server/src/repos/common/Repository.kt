package com.server.repos.common

import arrow.core.Either
import arrow.core.NonEmptyList
import org.bson.conversions.Bson

open class RecordID

typealias RepoRecordNEL<T> = NonEmptyList<RepoRecord<T>>
typealias WriteErrorNEL = NonEmptyList<WriteError>
interface Repository<T> {

    suspend fun findAll(pagination: Pagination? = null):
        Either<FindError, RepoRecordNEL<T>>
    suspend fun find(vararg predicates: Predicate?, pagination: Pagination? = null):
        Either<FindError, RepoRecordNEL<T>>
    suspend fun findOne(vararg predicate: Predicate?): Either<FindError, RepoRecord<T>>

    suspend fun insert(records: NonEmptyList<T>): WriteErrorNEL?
    suspend fun insertOne(record: T): WriteError?

    suspend fun delete(records: RepoRecordNEL<T>): WriteErrorNEL?
    suspend fun deleteOne(record: RepoRecord<T>): WriteError?
    suspend fun delete(vararg predicate: Predicate?): WriteErrorNEL?
    suspend fun deleteOne(vararg predicate: Predicate?): WriteError?

    suspend fun replace(records: NonEmptyList<Pair<RepoRecord<T>, T>>): WriteErrorNEL?
    suspend fun replaceOne(record: RepoRecord<T>, replacement: T): WriteError?
    suspend fun replaceOne(replacement: T, vararg predicate: Predicate?): WriteError?

    suspend fun update(records: NonEmptyList<Pair<RepoRecord<T>, Patch<T>>>): WriteErrorNEL?
    suspend fun updateOne(record: RepoRecord<T>, patch: Patch<T>): WriteError?
    suspend fun update(patch: Patch<T>, vararg predicate: Predicate?): WriteErrorNEL?
    suspend fun updateOne(patch: Patch<T>, vararg predicate: Predicate?): WriteError?

    suspend fun bulkWrite(operations: Iterable<WriteOperation<T>>): WriteErrorNEL?

//    // TODO: Replace with some kind of annotation reader/code generator
//    fun getIndices(): Map<String, Collection<FieldOption>>
}
