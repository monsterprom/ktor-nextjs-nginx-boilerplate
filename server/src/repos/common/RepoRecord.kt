package com.server.repos.common

sealed class RepoRecord<T> {

    abstract val id: RecordID
    abstract val doc: T

    abstract fun isReal(): Boolean
    abstract fun isEphemeral(): Boolean
    abstract fun <R> map(f: (T) -> R): RepoRecord<R>

    fun <R> flatMap(f: (T) -> R): R = f(doc)
    fun bind(): T = doc

    data class Real<T>(
        override val id: RecordID,
        override val doc: T
    ): RepoRecord<T>() {
        override fun isReal(): Boolean = true
        override fun isEphemeral(): Boolean = false

        override fun <R> map(f: (T) -> R): RepoRecord<R> = f(doc).record(id)
    }
}

fun <T> T.record(id: RecordID): RepoRecord<T> =
    RepoRecord.Real(id, this)
