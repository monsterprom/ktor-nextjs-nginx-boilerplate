package com.server.repos.common

import arrow.core.NonEmptyList
import kotlin.reflect.KProperty

enum class FieldOption { UNIQUE, OPTIONAL }
data class Pagination(val skip: Int, val limit: Int)

sealed class FindError {
    object NotFound: FindError()
    data class InvalidSchema(val message: String): FindError()
    data class Unknown(val message: String): FindError()
}


// Maybe simplified in the future to something more generic
// or some options might be removed in case of incompatibility
// with other supported databases
sealed class WriteError {
    data class AlienID(val message: String): WriteError()
    data class Timeout(val message: String): WriteError()
    data class Duplicate(val message: String): WriteError()
    data class Permitted(val message: String): WriteError()
    data class UnknownError(val message: String): WriteError()
}


sealed class WriteOperation<T> {
    data class Insert<T>(val record: T): WriteOperation<T>()

    data class DeleteRecord<T>(val record: RepoRecord<T>): WriteOperation<T>()
    data class DeleteOne<T>(val predicate: Predicate): WriteOperation<T>()
    data class DeleteMany<T>(val predicate: Predicate): WriteOperation<T>()

    data class ReplaceRecord<T>(val record: RepoRecord<T>, val replacement: T): WriteOperation<T>()
    data class Replace<T>(val predicate: Predicate, val replacement: T): WriteOperation<T>()

    data class UpdateRecord<T>(val record: RepoRecord<T>, val patch: Patch<T>): WriteOperation<T>()
    data class UpdateOne<T>(val predicate: Predicate, val patch: Patch<T>): WriteOperation<T>()
    data class UpdateMany<T>(val predicate: Predicate, val patch: Patch<T>): WriteOperation<T>()
}
