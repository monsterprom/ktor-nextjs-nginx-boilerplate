package com.server.repos.common

import kotlin.reflect.KProperty

sealed class Patch<T> {
    data class Set<T>(val values: Map<KProperty<T>, T>): Patch<T>() {
        constructor(vararg valPairs: Pair<KProperty<T>, T>):
            this(valPairs.toMap())
    }
}
