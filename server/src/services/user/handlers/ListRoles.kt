package com.server.services.user.handlers

import com.server.repos.Role
import com.server.repos.RoleRepo
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*
import org.koin.ktor.ext.inject

object ListRolesHandler {

    data class RoleData(
        val name: String,
        val slug: String,
        val description: String?,
        val permissions: List<String>
    ) {
        constructor(role: Role): this(
            name=role.name,
            slug=role.slug,
            description=role.description,
            permissions=role.permissions
        )
    }

    data class ResponseSuccess(
        val roles: List<RoleData>,
        val success: Boolean = true
    )

    fun Routing.listRoles() {
        val roleRepo by inject<RoleRepo>()

        get("/user/roles") {
            // TODO: Support pagination
//            call.respond(ResponseSuccess(
//                roleRepo.findAll().toList().map { RoleData(it) }
//            ))
        }
    }
}