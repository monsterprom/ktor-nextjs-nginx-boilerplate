package com.server.services.user.handlers

import arrow.core.Either
import arrow.core.extensions.either.apply.tupled
import arrow.core.left
import arrow.core.right

import com.server.repos.Role
import com.server.repos.RoleRepo
import com.server.util.Permissions
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.routing.*
import io.ktor.request.*
import io.ktor.response.*
import org.koin.ktor.ext.inject
import org.litote.kmongo.*

//object ListPermissionsHandler {
//    data class PermissionData(
//        val id: String,
//        val name: String,
//        val description: String
//    )
//
//    data class ListRolesResponse(
//        val permissions: List<PermissionData>,
//        val success: Boolean = true
//    )
//
//    fun Routing.listPermissions() {
//
//        get("/user/permissions") {
//            call.respond(ListRolesResponse(
//                Permissions.values().map { PermissionData(it.id, it.permName, it.description) }
//            ))
//        }
//    }
//}
