package com.server.services.user.handlers

//object DeleteRoleHandler {
//    data class Request(
//        val replaceWith: String?
//    )
//
//    data class ResponseSuccess(val success: Boolean = true)
//
//    enum class ErrorType { ROLE_NOT_FOUND, ROLE_NOT_SET, INTERNAL_ERROR }
//    sealed class ResponseError {
//        data class InternalError(
//            val success: Boolean = false,
//            val type: ErrorType = ErrorType.INTERNAL_ERROR
//        ): ResponseError()
//    }
//
//    private fun dropFromUsers(repo: UserRepo, role: Role): Either<ResponseError, List<User>> {
//        try {
//            repo.find(User::roles.contains(role.slug)).forEach {
//                println(it)
//            }
//        } catch (e: Exception) {
//            return ResponseError.RoleNotFound(e.toString()).left()
//        }
//        val users = repo.find(User::roles.contains(role.slug)).toList()
//        if (repo.pullRole(users, role.slug) is UpdateResult.Failed)
//            return ResponseError.RoleNotPulled(role.slug).left()
//
//        return users.right()
//    }
//
//    private fun assignRole(roleRepo: RoleRepo, userRepo: UserRepo, users: List<User>, role: String)
//    : Either<ResponseError, Either.Companion> {
//        if (users.isEmpty()) return Either.right()
//
//        roleRepo.findOne(Role::slug eq role)
//            ?: return ResponseError.RoleNotFound(role).left()
//
//        if (userRepo.assignRole(users, role) is UpdateResult.Failed)
//            return ResponseError.RoleNotAssigned(role).left()
//
//        return Either.right()
//    }
//
//    private fun deleteUsers(repo: UserRepo, role: Role): Either<ResponseError, Either.Companion> {
//        if (repo.delete(User::roles.contains(role.slug)) is DeleteResult.Failed)
//            return ResponseError.UsersNotDeleted(role.slug).left()
//
//        return Either.right()
//    }
//
//    fun Routing.deleteRole() {
//        val roleRepo by inject<RoleRepo>()
//        val userRepo by inject<UserRepo>()
//
//        post("/user/role/{slug}/delete") {
//            val slug = call.parameters["slug"] ?: "" // Won't be found anyway
//            IO.fx {
//                val request = !IO.effect { call.receive<Request>() }
//                val role = !roleRepo.deleteRole(slug)
//                if (request.replaceWith == null) {
//                    !deleteUsers(userRepo, role)
//                } else {
//                    val users = !dropFromUsers(userRepo, role)
//                    !assignRole(roleRepo, userRepo, users, request.replaceWith)
//                }
//
//                role
//            }.attempt().suspended().pipe {
//                when (it) {
//                    is Either.Left -> call.respond("Something failed")
//                    is Either.Right -> call.respond(ResponseSuccess())
//                }
//            }
//        }
//    }
//}
//
//
