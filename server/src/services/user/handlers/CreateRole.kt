package com.server.services.user.handlers

import arrow.core.Either
import arrow.core.computations.either
import arrow.core.left
import arrow.core.right

import com.server.repos.Role
import com.server.repos.RoleRepo
import com.server.util.Permissions
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.routing.*
import io.ktor.request.*
import io.ktor.response.*
import org.koin.ktor.ext.inject
import org.litote.kmongo.*
import kotlin.reflect.KProperty

//object CreateUserHandler {
//    data class Request(
//        val name: String,
//        val slug: String,
//        val description: String,
//        val permissions: List<String>
//    )
//
//    data class ResponseSuccess(val success: Boolean = true)
//
//    enum class ErrorType { BAD_REQUEST, FIELD_ERROR, INTERNAL_ERROR }
//    sealed class ResponseError {
//        data class BadRequest(
//            val message: String, val success: Boolean,
//            val type: ErrorType = ErrorType.BAD_REQUEST
//        ): ResponseError() {
//            constructor(permission: String):
//                this("Unknown permission '$permission'", false)
//        }
//
//        data class FieldValueTaken(
//            val field: String, val message: String, val success: Boolean,
//            val type: ErrorType = ErrorType.FIELD_ERROR
//        ): ResponseError() {
//            constructor(field: String):
//                this(field, "Role with a field '$field' already exists!", false)
//        }
//
//        data class InternalError(
//            val success: Boolean = false,
//            val type: ErrorType = ErrorType.INTERNAL_ERROR
//        ): ResponseError()
//    }
//
//    private fun validatePermissions(permissions: List<String>)
//    : Either<ResponseError, Either.Companion> {
//        val perm = permissions.find { Permissions.fromId(it) == null }
//        return if (perm != null) ResponseError.BadRequest(perm).left() else Either.right()
//    }
//
//    private fun <T> uniqueField(repo: RoleRepo, prop: KProperty<T>, value: T)
//    : Either<ResponseError, Either.Companion> =
//        if (repo.findOne(prop eq value) == null)
//            Either.right()
//        else
//            ResponseError.FieldValueTaken(prop.name).left()
//
//    fun Routing.createRole() {
//        val roleRepo by inject<RoleRepo>()
//
//        post("/user/role/create") {
//            val (name, slug, description, permissions) = call.receive<Request>()
//            val validation = either<ResponseError, Role> {
//                !validatePermissions(permissions)
//                !uniqueField(roleRepo, Role::slug, slug)
//                !uniqueField(roleRepo, Role::name, name)
//                Role(name, slug, description, permissions)
//            }
//
//            when (validation) {
//                is Either.Left -> call.respond(validation.a)
//                is Either.Right -> {
//                    val insertRes = roleRepo.insertOne(validation.b)
//                    if (insertRes is InsertOneResult.Failed) {
//                        // TODO: Log error
//                        call.response.status(HttpStatusCode.InternalServerError)
//                        call.respond(ResponseError.InternalError())
//                    } else {
//                        call.respond(ResponseSuccess())
//                    }
//                }
//            }
//        }
//    }
//}
//
//
