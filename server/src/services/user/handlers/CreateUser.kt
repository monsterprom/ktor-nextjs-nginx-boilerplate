package com.server.services.user.handlers

//data class CreateUserRequest(
//    val name: String,
//    val slug: String,
//    val description: String,
//    val permissions: List<String>
//)
//
//object UserValidated
//data class CreationSuccess(val success: Boolean = true)
//
//enum class CreateUserErrorType { BAD_REQUEST, FIELD_ERROR, INTERNAL_ERROR }
//sealed class UserCreationError {
//    data class BadRequest(
//        val message: String, val success: Boolean,
//        val type: CreateUserErrorType = CreateUserErrorType.BAD_REQUEST
//    ): UserCreationError() {
//        constructor(permission: String):
//            this("Unknown permission '$permission'", false)
//    }
//
//    data class FieldValueTaken(
//        val field: String, val message: String, val success: Boolean,
//        val type: CreateUserErrorType = CreateUserErrorType.FIELD_ERROR
//    ): UserCreationError() {
//        constructor(field: String):
//            this(field, "Role with a field '$field' already exists!", false)
//    }
//
//    data class InternalErrorUser(
//        val success: Boolean = false,
//        val type: CreateUserErrorType = CreateUserErrorType.INTERNAL_ERROR
//    ): UserCreationError()
//}
//
//fun Routing.createUser() {
//    val userRepo by inject<UserRepo>()
//
//    post("/user/create") {
////        val (name, slug, description, permissions) = call.receive<CreateUserRequest>()
////        val validation = tupled(
////            when (val unknownPerm = permissions.find { Permissions.fromId(it) == null }) {
////                is String -> UserCreationError.BadRequest(unknownPerm).left()
////                else -> UserDataValidated.right()
////            },
////            when (roleRepo.findOne(Role::slug eq slug)) {
////                is Role -> UserCreationError.FieldValueTaken("slug").left()
////                else -> UserDataValidated.right()
////            },
////            when (roleRepo.findOne(Role::name eq name)) {
////                is Role -> UserCreationError.FieldValueTaken("name").left()
////                else -> UserDataValidated.right()
////            }
////        )
////
////        when (validation) {
////            is Either.Left -> call.respond(validation.a)
////            else -> {
////                val role = Role(name, slug, description, permissions)
////                val insertRes = roleRepo.insertOne(role)
////                if (insertRes is InsertOneResult.Failed) {
////                    // TODO: Log error
////                    call.response.status(HttpStatusCode.InternalServerError)
////                    call.respond(UserCreationError.InternalErrorUser())
////                } else {
////                    call.respond(UserCreationSuccess())
////                }
////            }
////        }
//    }
//}
//
