package com.server

import com.server.repos.RoleRepo
import com.server.repos.RoleRepoMongo
import com.server.repos.user.UserRepo
import com.server.repos.user.UserRepoMongo
import com.server.repos.backend.mongo.MongoConnection
import com.server.services.user.userRoutes
import com.server.util.JWTConfig
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.auth.*
import io.ktor.gson.*
import io.ktor.features.*
import io.ktor.locations.*
import org.slf4j.event.*

import io.ktor.auth.jwt.jwt
import io.ktor.util.date.*

import org.koin.dsl.module
import org.koin.ktor.ext.*

import org.slf4j.LoggerFactory
import ch.qos.logback.classic.Level as ClassicLevel
import ch.qos.logback.classic.Logger

fun main(args: Array<String>): Unit = io.ktor.server.cio.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    val root: Logger = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME) as Logger
    root.level = ClassicLevel.INFO

    install(Authentication) {
        JWTConfig.read(environment)
        jwt {
            verifier(JWTConfig.makeVerifier())
            realm = environment.config.property("jwt.realm").getString()
            validate { null }
        }
    }

    install(Koin) {
        fileProperties("/extended.properties")
        modules(modules)
    }

    install(ContentNegotiation) {
        gson {
        }
    }

    install(Locations)

    install(CallLogging) {
        level = Level.INFO
        filter { call -> call.request.path().startsWith("/") }
    }

    install(CORS) {
        method(HttpMethod.Options)
        method(HttpMethod.Put)
        method(HttpMethod.Delete)
        method(HttpMethod.Patch)
        header(HttpHeaders.Authorization)
        header("Access-Control-Allow-Origin")
        header("Access-Control-Allow-Headers")
        header("Content-Type")
        allowCredentials = true

        anyHost() // @TODO: Don't do this in production if possible. Try to limit it.
    }

    routing {
        userRoutes()

        get("/") {
            val expdate = GMTDate().plus(3600 * 720_000)
            call.response.cookies.append("TestCookie", "1", expires = expdate)
            call.respondText("HELLO WORLD!", contentType = ContentType.Text.Plain)
        }
    }
}


val modules = module(createdAtStart = true) {
    single {
        MongoConnection.fromProps(
            getProperty("db_host"),
            getProperty("db_user"),
            getProperty("db_password"),
            getProperty("db_name")
        )
    }

    // Cast needed to inject by interface
    single { UserRepoMongo(get<MongoConnection>()) as UserRepo }
    single { RoleRepoMongo(get<MongoConnection>()) as RoleRepo }
}
