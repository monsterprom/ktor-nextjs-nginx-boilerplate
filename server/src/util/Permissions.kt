package com.server.util

enum class Permissions(val id: String, val permName: String, val description: String) {
    READ_USERS(
        "read-users",
        "Просмотр пользователей",
        "Дает доступ к личным данным пользователей"
    ),
    MANAGE_USERS(
        "manage-users",
        "Управление пользователями",
        "Разрешает пользователю создавать и изменять данные пользователей"
    );

    companion object {
        private val map  = Permissions.values().associateBy(Permissions::id)
        fun fromId(id: String) = map[id]
    }
}