package com.server.util

import com.auth0.jwt.*
import com.auth0.jwt.algorithms.*
import io.ktor.application.*
import java.util.*

data class JWTData(
    val validDuration: Int,
    val issuer: String,
    val algo: Algorithm
)

object JWTConfig {
    private var config: JWTData? = null
    private var verifier: JWTVerifier? = null

    fun read(environment: ApplicationEnvironment) {
        val secret = environment.config.property("jwt.secret").getString()
        val duration = environment.config.property("jwt.duration").getString().toInt()
        val issuer = environment.config.property("jwt.issuer").getString()

        config = JWTData(
            duration,
            issuer,
            Algorithm.HMAC512(secret)
        )
    }

    fun makeVerifier(): JWTVerifier = verifier ?: JWT
        .require(config!!.algo)
        .withIssuer(config!!.issuer)
        .build().also { verifier = it }

    fun makeToken(): String = JWT.create()
        .withSubject("auth")
        .withIssuer(config!!.issuer)
        .withExpiresAt(calcExpiration(config!!.validDuration))
        .sign(config!!.algo)

    private fun calcExpiration(duration: Int) = Date(System.currentTimeMillis() + duration)
}
