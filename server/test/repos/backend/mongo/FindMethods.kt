package com.server.repos.backend.mongo

import arrow.core.Either
import arrow.core.NonEmptyList
import com.server.repos.common.*
import org.bson.Document
import org.junit.jupiter.api.*


@DisplayName("Mongo repository find methods")
class FindMethodsTest {

    @Nested
    @DisplayName("check findOne method")
    inner class FindOneMethod {
        @Test
        fun `on existing correct document`() = withConnection { conn ->
            val testDocument = TestDocument("krakov", emptyList())
            val dataClass = TestDocument::class.java

            conn.db.getCollection(MongoRepository.genCollectionName(dataClass), dataClass)
                .insertOne(testDocument)

            val repo = TestRepo(conn)
            val result = repo.findOne(TestDocument::username eq "krakov")
            assertRecord(result, testDocument)
        }

        @Test
        fun `on existing document with invalid schema`() = withConnection { conn ->
            val testDocument = Document("username", "krakov")
                .append("roles", "string")

            val dataClass = TestDocument::class.java
            conn.db.getCollection(MongoRepository.genCollectionName(dataClass))
                .insertOne(testDocument)

            val repo = TestRepo(conn)
            val result = repo.findOne(TestDocument::username eq "krakov")
            assertLeft<FindError, RepoRecord<TestDocument>, FindError.InvalidSchema>(result)
        }

        @Test
        fun `on non-existant document`() = withConnection { conn ->
            val repo = TestRepo(conn)
            val result = repo.findOne(TestDocument::username eq "krakov")
            assertLeft<FindError, RepoRecord<TestDocument>, FindError.NotFound>(result)
        }
    }

    @Nested
    @DisplayName("check findAll method")
    inner class FindAllMethod {
        @Test
        fun `on existing documents`() = withConnection { conn ->
            val testDocuments = listOf(
                TestDocument("krakov", emptyList()),
                TestDocument("portyanko", listOf("admin", "moderator")),
                TestDocument("frolov", listOf("moderator")),
            )
            val dataClass = TestDocument::class.java

            conn.db.getCollection(MongoRepository.genCollectionName(dataClass), dataClass)
                .insertMany(testDocuments)

            val repo = TestRepo(conn)
            val result = repo.findAll()
            assertRecordNEL(result, testDocuments)
        }

        @Test
        fun `on existing documents with a invalid schema`() = withConnection { conn ->
            val testDocuments = listOf(
                TestDocument("krakov", emptyList()),
                TestDocument("portyanko", listOf("admin", "moderator")),
                TestDocument("frolov", listOf("moderator")),
            )
            val dataClass = TestDocument::class.java

            conn.db.getCollection(MongoRepository.genCollectionName(dataClass), dataClass)
                .insertMany(testDocuments)

            conn.db.getCollection(MongoRepository.genCollectionName(dataClass))
                .insertOne(Document("username", "jhon"))

            val repo = TestRepo(conn)
            val result = repo.findAll()
            assertLeft<FindError, RepoRecordNEL<TestDocument>, FindError.InvalidSchema>(result)
        }

        @Test
        fun `on empty collection`() = withConnection { conn ->
            val repo = TestRepo(conn)
            val result = repo.findAll()
            assertLeft<FindError, RepoRecordNEL<TestDocument>, FindError.NotFound>(result)
        }
    }

    @Nested
    @DisplayName("check find method")
    inner class FindMethod {
        @Test
        fun `on existing documents`() = withConnection { conn ->
            val testDocuments = listOf(
                TestDocument("krakov", emptyList()),
                TestDocument("portyanko", listOf("admin", "moderator")),
                TestDocument("frolov", listOf("moderator")),
            )
            val dataClass = TestDocument::class.java

            conn.db.getCollection(MongoRepository.genCollectionName(dataClass), dataClass)
                .insertMany(testDocuments)

            val repo = TestRepo(conn)
            assertRecordNEL(
                repo.find(TestDocument::username eq "portyanko"),
                listOf(testDocuments[1])
            )
            assertRecordNEL(
                repo.find(TestDocument::roles.contains("moderator")),
                listOf(testDocuments[1], testDocuments[2])
            )
        }

        @Test
        fun `on existing documents with a invalid schema`() = withConnection { conn ->
            val testDocuments = listOf(
                TestDocument("krakov", emptyList()),
                TestDocument("portyanko", listOf("admin", "moderator")),
                TestDocument("frolov", listOf("moderator")),
            )
            val dataClass = TestDocument::class.java

            conn.db.getCollection(MongoRepository.genCollectionName(dataClass), dataClass)
                .insertMany(testDocuments)
            conn.db.getCollection(MongoRepository.genCollectionName(dataClass))
                .insertOne(Document("roles", listOf("moderator")))

            val repo = TestRepo(conn)
            assertRecordNEL(
                repo.find(TestDocument::username eq "portyanko"),
                listOf(testDocuments[1])
            )
            assertLeft<FindError, RepoRecordNEL<TestDocument>, FindError.InvalidSchema>(
                repo.find(TestDocument::roles.contains("moderator")),
            )
        }

        @Test
        fun `on empty query result`() = withConnection { conn ->
            val testDocuments = listOf(
                TestDocument("krakov", emptyList()),
                TestDocument("portyanko", listOf("admin", "moderator")),
                TestDocument("frolov", listOf("moderator")),
            )
            val dataClass = TestDocument::class.java

            conn.db.getCollection(MongoRepository.genCollectionName(dataClass), dataClass)
                .insertMany(testDocuments)

            val repo = TestRepo(conn)
            assertLeft<FindError, RepoRecordNEL<TestDocument>, FindError.NotFound>(
                repo.find(TestDocument::roles.contains("junior")),
            )
        }

        @Test
        fun `on empty collection`() = withConnection { conn ->
            val repo = TestRepo(conn)
            val result = repo.find(TestDocument::username eq "krakov")
            assertLeft<FindError, RepoRecordNEL<TestDocument>, FindError.NotFound>(result)
        }
    }
}