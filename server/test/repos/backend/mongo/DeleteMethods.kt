package com.server.repos.backend.mongo

import com.server.repos.common.eq
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

@DisplayName("Mongo repository delete methods")
class DeleteMethodsTest {

    @Nested
    @DisplayName("check deleteOne method")
    inner class InsertOneMethod {
        @Test
        fun `insert a valid document`() = withConnection { conn ->
            val testDocument = TestDocument("krakov", emptyList())
            val repo = TestRepo(conn)

            Assertions.assertEquals(repo.insertOne(testDocument), null)
            assertRecord(
                repo.findOne(TestDocument::username eq "krakov"),
                testDocument
            )
        }
    }
}