package com.server.repos.backend.mongo

import arrow.core.Either
import arrow.syntax.function.pipe
import com.server.repos.common.RepoRecord
import com.server.repos.common.RepoRecordNEL
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import java.lang.AssertionError
import java.util.*

fun withConnection(f: suspend (MongoConnection) -> Unit) {
    val mongoUri = "mongodb+srv://testing:5PNOGloKPYqWold5@cluster0.pvbsj.mongodb.net/?retryWrites=true&w=majority"
    val uuid = UUID.randomUUID().toString()
    val conn = MongoConnection.fromUri(mongoUri, uuid)

    try {
        runBlocking { f(conn) }
    } finally {
        conn.db.drop()
    }
}

inline fun <A, B, reified C> assertLeft(either: Either<A, B>) = when (either) {
    is Either.Left -> either.a is C
    else -> false
}.pipe {
    it || throw AssertionError("expected:<${C::class.java}> but was $either")
}

inline fun <A, reified B> assertRight(either: Either<A, B>, value: B) = when (either) {
    is Either.Right -> either.b == value
    else -> false
}.pipe {
    it || throw AssertionError("expected:<${B::class.java}> but was $either")
}

inline fun <A, reified B> assertRecord(either: Either<A, RepoRecord<B>>, record: B) = when (either) {
    is Either.Right -> either.b.bind() == record
    else -> false
}.pipe {
    it || throw AssertionError("expected:<${B::class.java}> but was $either")
}

inline fun <A, reified B> assertRecordNEL(either: Either<A, RepoRecordNEL<B>>, records: Iterable<B>)
= when (either) {
    is Either.Right -> {
        println("DIFFFFF")
        println(either.b.map { it.bind() }.minus(records).size)
        println(either.b.map { it.bind() }.minus(records))
        either.b.map { it.bind() }.minus(records).isEmpty()
    }
    else -> false
}.pipe {
    it || throw AssertionError("expected:<${B::class.java}> but was $either")
}

@DisplayName("Common mongodb related")
class CommonTest {
    @Test
    fun `check canonical collection names`() {
        data class User(val nomatter: String)
        data class UserRole(val nomatter: String)
        data class Document(val nomatter: String)
        data class Artist(val nomatter: String)
        data class Equipment(val nomatter: String)

        Assertions.assertEquals(MongoRepository.genCollectionName(User::class.java), "users")
        Assertions.assertEquals(MongoRepository.genCollectionName(UserRole::class.java), "user.roles")
        Assertions.assertEquals(MongoRepository.genCollectionName(Document::class.java), "documents")
        Assertions.assertEquals(MongoRepository.genCollectionName(Artist::class.java), "artists")
        Assertions.assertEquals(MongoRepository.genCollectionName(Equipment::class.java), "equipment")
    }
}