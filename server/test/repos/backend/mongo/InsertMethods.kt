package com.server.repos.backend.mongo

import arrow.core.NonEmptyList
import arrow.core.nel
import com.server.repos.common.eq
import com.tyro.oss.arbitrater.arbitrary
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.litote.kmongo.findOne
import kotlin.random.Random

@DisplayName("Mongo repository insert methods")
class InsertMethodsTest {

    @Nested
    @DisplayName("check insertOne method")
    inner class InsertOneMethod {
        @Test
        fun `insert a valid document`() = withConnection { conn ->
            val testDocument = TestDocument("krakov", emptyList())
            val repo = TestRepo(conn)

            assertEquals(repo.insertOne(testDocument), null)
            assertRecord(
                repo.findOne(TestDocument::username eq "krakov"),
                testDocument
            )
        }
    }

    @Nested
    @DisplayName("check insert method")
    inner class InsertMethod {
        @Test
        fun `insert N valid documents`() = withConnection { conn ->
            val repo = TestRepo(conn)

            (1..5)
                .fold(emptyList(), { acc: List<TestDocument>, _: Int ->
                    val docs = (1..Random.nextInt(1, 1000))
                        .map { arbitrary() as TestDocument }
                    val all = acc + docs

                    assertEquals(repo.insert(NonEmptyList.fromListUnsafe(docs)), null)
                    assertRecordNEL(repo.findAll(), all)
                    all
                })
        }
    }
}
