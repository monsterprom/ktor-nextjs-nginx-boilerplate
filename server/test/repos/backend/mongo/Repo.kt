package com.server.repos.backend.mongo

import com.server.repos.common.Repository

data class TestDocument(
    val username: String,
    val roles: List<String>,
)

class TestRepo(conn: MongoConnection):
    Repository<TestDocument>,
    MongoRepository<TestDocument>(conn, TestDocument::class.java)
