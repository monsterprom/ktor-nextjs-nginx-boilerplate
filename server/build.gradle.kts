import org.jetbrains.kotlin.gradle.dsl.Coroutines
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val logback_version: String by project
val ktor_version: String by project
val koin_version: String by project
val kotlin_version: String by project
val jvm_target: String by project
val arrow_version: String by project

plugins {
    application
    kotlin("jvm") version "1.4.21"
    kotlin("kapt") version "1.4.21"
    id("com.github.johnrengelman.shadow") version "5.0.0"
}

group = "com.server"
version = "0.0.1"

application {
    mainClassName = "io.ktor.server.cio.EngineMain"
    applicationDefaultJvmArgs = listOf(
        "-server",
        "-Dio.ktor.development=true"
    )
}

repositories {
    mavenLocal()
    jcenter()
    maven { url = uri("https://jitpack.io") }
    maven { url = uri("https://kotlin.bintray.com/ktor") }
    maven { url = uri("https://dl.bintray.com/arrow-kt/arrow-kt/") }
    maven { url = uri("https://oss.jfrog.org/artifactory/oss-snapshot-local/") }
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version")
    implementation("org.jetbrains.kotlin:kotlin-reflect:$kotlin_version")

    implementation("io.ktor:ktor-server-cio:$ktor_version")
    implementation("ch.qos.logback:logback-classic:$logback_version")
    implementation("io.ktor:ktor-server-core:$ktor_version")
    implementation("io.ktor:ktor-auth:$ktor_version")
    implementation("io.ktor:ktor-auth-jwt:$ktor_version")
    implementation("io.ktor:ktor-gson:$ktor_version")
    implementation("io.ktor:ktor-locations:$ktor_version")
    implementation("io.ktor:ktor-metrics:$ktor_version")

    implementation("org.litote.kmongo:kmongo:4.2.3")

    implementation("org.koin:koin-ktor:$koin_version")

    implementation("io.arrow-kt:arrow-fx:$arrow_version")
    implementation("io.arrow-kt:arrow-optics:$arrow_version")
    implementation("io.arrow-kt:arrow-syntax:$arrow_version")

    implementation("com.github.cesarferreira:kotlin-pluralizer:1.0.0")

//    testImplementation("io.ktor:ktor-server-tests:$ktor_version")
    implementation("com.tyro.oss:arbitrater:1.0.0")
    testImplementation(platform("org.junit:junit-bom:5.7.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")

    kapt("io.arrow-kt:arrow-meta:$arrow_version")
}

kotlin.sourceSets["main"].kotlin.srcDirs("src")
kotlin.sourceSets["test"].kotlin.srcDirs("test")

sourceSets["main"].resources.srcDirs("resources")
sourceSets["test"].resources.srcDirs("testresources")

kapt {
    mapDiagnosticLocations = true
}

tasks.withType<KotlinCompile>().all {
    kotlinOptions {
        jvmTarget = jvm_target
        freeCompilerArgs = listOf("-Xallow-kotlin-package")
    }
}

tasks.withType<Jar> {
    archiveFileName.set("app.jar")
    manifest {
        attributes(
            mapOf(
                "Main-Class" to application.mainClassName
            )
        )
    }
}

tasks.test {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}
