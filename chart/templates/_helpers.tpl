{{/*
Expand the name of the chart.
*/}}
{{- define "appchart.name" -}}
{{- default .root.Chart.Name (printf "%s-%s" .root.Chart.Name .service) | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified service name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "appchart.fullname" -}}
{{- if .current.fullnameOverride }}
{{- .current.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .root.Chart.Name .nameOverride }}
{{- if contains $name .root.Release.Name }}
{{- printf "%s-%s" .root.Release.Name .service | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s-%s" .root.Release.Name $name .service | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "appchart.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "appchart.labels" -}}
helm.sh/chart: {{ include "appchart.chart" .root }}
{{ include "appchart.selectorLabels" . }}
{{- if .root.Chart.AppVersion }}
app.kubernetes.io/version: {{ .root.Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .root.Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "appchart.selectorLabels" -}}
app.kubernetes.io/name: {{ include "appchart.name" . }}
app.kubernetes.io/instance: {{ .root.Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "appchart.serviceAccountName" -}}
{{- if .current.serviceAccount.create }}
{{- default (include "appchart.fullname" .) .current.serviceAccount.name }}
{{- else }}
{{- default "default" .current.serviceAccount.name }}
{{- end }}
{{- end }}
