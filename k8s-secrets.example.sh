#!/bin/bash

DOCKER_REGISTRY_SERVER=https://gitlab.com/
DOCKER_USER=your gitlab token username
DOCKER_EMAIL=your gitlab email
DOCKER_PASSWORD=your gitlab token password
NAMESPACE=project

kubectl create secret \
  --namespace $NAMESPACE \
  docker-registry gitlab-registry \
  --docker-server=$DOCKER_REGISTRY_SERVER \
  --docker-username=$DOCKER_USER \
  --docker-password=$DOCKER_PASSWORD \
  --docker-email=$DOCKER_EMAIL

